import Ember from 'ember';
import ResizeServiceInitializer from 'jira-reports/initializers/resize-service';
import { module, test } from 'qunit';

let application;

module('Unit | Initializer | resize service', {
  beforeEach() {
    Ember.run(function() {
      application = Ember.Application.create();
      application.deferReadiness();
    });
  }
});

// Replace this with your real tests.
test('it works', function(assert) {
  ResizeServiceInitializer.initialize(application);

  // you would normally confirm the results of the initializer here
  assert.ok(true);
});
