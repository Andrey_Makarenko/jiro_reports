import Ember from 'ember';

export default Ember.Component.extend({
    resize: Ember.inject.service(),
    classNames: ['nav toggle'],
    click: function () {
        Ember.$("body").toggleClass("nav-md nav-sm");
        this.get('resize').resize();
    }
});
