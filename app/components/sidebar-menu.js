import Ember from 'ember';

export default Ember.Component.extend({
    resize: Ember.inject.service(),
    classNameBindings: ['isActive:active:'],
    isActive: false,
    tagName: 'li',
    click: function () {
        let resize = this.get('resize');
        var element = Ember.$(this.element);
        if (this.isActive) {
            Ember.set(this, 'isActive', false);
            Ember.$("ul:first", element).slideUp(function () {
                resize.resize();
            });
        } else {
            Ember.set(this, 'isActive', true);
            Ember.$("ul:first", element).slideDown(function () {
                resize.resize();
            })
        }
    }
});
