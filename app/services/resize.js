import Ember from 'ember';

export default Ember.Service.extend({
    init: function () {
        var self = this;
        Ember.run.scheduleOnce('afterRender', self, self.resize);
        Ember.$(window).resize(function () {
            Ember.run.debounce(self, self.resize, 100);
        });
    },
    resize: function () {
        Ember.$(".right_col").css("min-height", Ember.$(window).height());
        var e = Ember.$("body").outerHeight(), t = Ember.$("body").hasClass("footer_fixed") ?
        0 : Ember.$("footer").height(), n = Ember.$(".left_col").eq(1).height() + Ember.$(".sidebar-footer").height(), i = n > e ? n : e;
        i -= Ember.$(".nav_menu").height() + t, Ember.$(".right_col").css("min-height", i)
    }
});
