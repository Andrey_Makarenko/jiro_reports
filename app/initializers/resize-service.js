import Resize from '../services/resize'
export function initialize(application) {
    application.register('resize:main', Resize, {singleton: true});
    application.inject('component', 'Resize', 'service:resize');
    application.inject('controller', 'Resize', 'service:resize');
}

export default {
    name: 'resize-service',
    initialize
};